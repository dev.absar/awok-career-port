

$(function(){

	$(window).on( 'load', function(){
		// Loader Close
		setTimeout( function(){
			$('.loading').hide();
		}, 1000 );

	})

	// Smooth Scroll
	$('a[href*=\\#]').on('click', function(event){     
	    event.preventDefault();
	    $('html,body').animate({scrollTop: $('.spacer.s1').offset().top }, 500);
	});


	// init controller
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

	// build scenes
	new ScrollMagic.Scene({triggerElement: "#parallax1" , duration: 2000, offset: 0 })
					.setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})
					.addTo(controller);
	new ScrollMagic.Scene({triggerElement: "#parallax2"})
					.setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})
					.addTo(controller);
	new ScrollMagic.Scene({triggerElement: "#parallax3"})
					.setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})
					.addTo(controller);

})



$(function(){

	// Life at
	flickr( '951a35035bd7547170ddb48ff8b6801d', '72157684017616846', '155118645@N05', $('.js_gallery'),1900 );
	// Fun at
	flickr( '951a35035bd7547170ddb48ff8b6801d', '72157684117726535', '155118645@N05', $('.js_gallery_tow'), 1900 );

	function flickr( key, photo_set, user, dom, timeout ){
		$.ajax({
				url: "https://api.flickr.com/services/rest/",
				data: {
				method: "flickr.photosets.getPhotos",
				api_key: key,
				photoset_id: photo_set,
				user_id: user,
				format: "json",
				nojsoncallback: 1
			},
			success: function (response) {
				$.each(response.photoset.photo, function (index, value) {
					var url = 'https://farm' + value.farm + '.staticflickr.com/' + value.server + '/' + value.id + '_' + value.secret + '.jpg';
					$(dom).append( $('<li />').append( $('<img />').attr( 'src', url ) ) );
				});

				$(dom).owlCarousel({
				    loop:true,
				    margin:10,
				    nav:true,
				    dots: true
				})

			}
		});
	}


	// Tabs
	$('.aw_tabs ol li').bind('click', function( e ){
		e.preventDefault();
		if( $(this).parent().parent().hasClass('nojs') ){ 
			return false;

		} else {
			$('.aw_tabs > ol > li').removeClass('active');
			$('.aw_tabs > ul > li').hide();
			$('.aw_tabs > ul > li').eq( $(this).index() ).show();
			$(this).addClass('active');

		}
	});


})



$(function(){   

    // var $window = $(window);
    // var $scoll = $('body');
    // var scrollTime = 1;
    // var scrollDistance = 120;

    // var scrollTop = $scoll.scrollTop();

    // $window.on("mousewheel", function(event){

    //     event.preventDefault()  

    //     var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3
    //     scrollTop = scrollTop - parseInt(delta*scrollDistance)
    //     scrollTop = Math.max(0, Math.min($scoll[0].scrollHeight, scrollTop))

    //     TweenMax.to($scoll, scrollTime, {
    //         scrollTo : { y: scrollTop, autoKill:true },
    //             ease: Power1.easeOut,
    //             overwrite: 5                            
    //         })

    // })


	var startRotation = setInterval(rotateImages, 3700); 

	function rotateImages() {
		var curPhoto = $("ul.ll-active");
		var nextPhoto = curPhoto.next();
		if(nextPhoto.length == 0) {
			nextPhoto = $("ul.logo-list:first");
			$('li.hidden-client').removeClass('hidden-client');
		}

		curPhoto.removeClass('ll-active').addClass('ll-hidden');
		nextPhoto.removeClass('ll-hidden').addClass('ll-active');  
	}

});
